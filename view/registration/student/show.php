<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'studentreg' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

use App\registration\student\Student;
use App\registration\Utility\Utility;

$ccc = new Student();
$info = $ccc->show($_GET['id']);
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Student Info</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            th,td {
                border: 3px solid #DDD;
            }
        </style>
    </head>
    <body>

        <section>
            <div class="container">

                <h2 class="text-green-lt text-center">Student info</h2>
                <table class="table table-striped">
                    <thead style="border: 2px solid #CCC" class="text-info">
                        <tr >
                        <th>ID</th>
                        <th>Student name</th>
                        <th>Email</th>
                        <th>Father name</th>
                        <th>mother name</th>
                        <th>Reg. Date</th>
                        <th>Address</th>
                        <th>Gender</th>
                        <th>Department</th>
                        <th>Hobby</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-success">
                        <td><?php echo $info['id'] ?></td>
                        <td><?php echo $info['stname'] ?></td>
                        <td><?php echo $info['email'] ?></td>
                        <td><?php echo $info['fname'] ?></td>
                        <td><?php echo $info['mname'] ?></td>
                        <td><?php echo $info['date'] ?></td>
                        <td><?php echo $info['address'] ?></td>
                        <td><?php echo $info['gender'] ?></td>
                        <td><?php echo $info['contact'] ?></td>
                        <td><?php echo $info['department'] ?></td>
                        <td><?php echo $info['hobby'] ?></td>
                    </tr>
                </tbody>
            </table>
            <nav><a href="index.php">Go to Home</a></nav>

        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>


    </body>
</html>