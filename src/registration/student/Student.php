<?php

namespace App\registration\student;

use App\registration\Utility\Utility;

class Student {

    public $id = "";
    public $stname = "";
    public $email = "";
    public $fname = "";
    public $mname = "";
    public $date = "";
    public $address = "";
    public $gender = "";
    public $contact = "";
    public $department = "";
    public $hobby = "";
    public $deleted_at = "";

    public function __construct($data = false) {
        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }

        $this->stname = $data['stname'];
        $this->email = $data['email'];
        $this->fname = $data['fname'];
        $this->mname = $data['mname'];
        $this->date = $data['date'];
        $this->address = $data['address'];
        $this->gender = $data['gender'];
        $this->contact = $data['contact'];
        $this->department = $data['department'];
        $this->hobby = $data['hobby'];

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("university") or die("Cannot select database.");
    }

    public function index($data = array()) {
        
        $whereClause = " AND 1=1";
        if(array_key_exists('search',$data)){
            $whereClause .= " AND stname LIKE '%".$data['search']."%'";
            $whereClause .= " OR contact LIKE '%".$data['search']."%'";
            $whereClause .= " OR fname LIKE '%".$data['search']."%'";
        }else{
        $filter = $data;
            if(is_array($filter) && count($filter) > 0){

              if(array_key_exists('filterName', $filter) && !empty($filter['filterName']) ){
                  $whereClause .= " AND stname LIKE '%".$filter['filterName']."%'";
              }

              if(array_key_exists('filterContact', $filter)  && !empty($filter['filterContact']) ){
                  $whereClause .= " AND contact LIKE '%".$filter['filterContact']."%'";
              }

            }
        }
        
        $_student = array();
        $query = "SELECT * FROM `student` WHERE `deleted_at` IS NULL".$whereClause;
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_student[] = $row;
        }
        return $_student;
    }

    public function show($id = false) {


        $query = "SELECT * FROM `student` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function store() {
        $_check = $this->hobby;
        $_hobby = implode(",", $_check);

        $_hobby = trim($_hobby);

        $query = "INSERT INTO `university`.`student` (`stname`, `email`, `fname`, `mname`, `date`, `address`, `gender`, `contact`, `department`, `hobby`) VALUES ('" . $this->stname . "', '" . $this->email . "', '" . $this->fname . "', '" . $this->mname . "', '" . $this->date . "', '" . $this->address . "', '" . $this->gender . "', '" . $this->contact . "', '" . $this->department . "', '$_hobby');";
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Student Info is inserted successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function update() {


        $_hobby = implode(",", $this->hobby);
        $_hobby = trim($_hobby);

        $query = "UPDATE `university`.`student` SET `stname` = '" . $this->stname . "', `email` = '" . $this->email . "', `fname` = '" . $this->fname . "', `mname` = '" . $this->mname . "', `date` = '" . $this->date . "', `address` = '" . $this->address . "', `contact` = '" . $this->contact . "', `department` = '" . $this->department . "', `hobby` = '$_hobby' WHERE `student`.`id` =" . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Student info updated successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function delete($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }


        $query = "DELETE FROM `student` WHERE `student`.`id` = " . $id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Student Detail is successfully Deleted");
        } else {
            Utility::message("can not delete");
        }
        Utility::redirect('index.php');
    }
    
     public function deletemultiple($ids = array()){
       
            if(is_array($ids) && count($ids) > 0){

                 //Utility::dd($ids);
                 $_ids = implode(',',$ids);



                 $query = "DELETE FROM `university`.`student` WHERE `student`.`id` IN($_ids) ";

                 //Utility::dd( $query);
                 $result = mysql_query($query);

                 if($result){
                     Utility::message("Students are deleted successfully.");
                 }else{
                     Utility::message(" Cannot delete.");
                 }

                 Utility::redirect('index.php');
             }else{
                 Utility::message('No id avaiable. Sorry !');
                 return Utility::redirect('index.php');
             }
    }

    public function trash($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }
        $this->id = $id;
        $this->deleted_at = time();

        $query = "UPDATE `university`.`student` SET `deleted_at` = '" . $this->deleted_at . "' WHERE `student`.`id` = " . $this->id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Student Detail is successfully Trashed");
        } else {
            Utility::message("Can not Trash");
        }
        Utility::redirect('index.php');
    }

    public function trashed() {
        $std = array();

        $query = "SELECT * FROM `student` WHERE `deleted_at` IS NOT NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $std[] = $row;
        }

        return $std;
    }

    public function recover($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }
        $this->id = $id;

        $query = "UPDATE `university`.`student` SET `deleted_at` = NULL WHERE `student`.`id` = " . $this->id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Student is successfully Recovered");
        } else {
            Utility::message("Can not Recovered");
        }
        Utility::redirect('index.php');
    }

    public function recovermultiple($ids = array()) {
        if (is_array($ids) && count($ids) > 0) {

            $_ids = implode(',', $ids);


            $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
            $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");

            $query = "UPDATE `university`.`student` SET `deleted_at` = NULL WHERE `student`.`id` IN($_ids) ";

            $result = mysql_query($query);
            if ($result) {
                Utility::message("Students are successfully Recovered");
            } else {
                Utility::message("Can not Recover");
            }
            Utility::redirect('index.php');
        } else {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }
    }

}
